from add_message import *

from Yahoo_first_step import pull_historical_data
import datetime

def p2f(x):
    return float(x.strip('%'))/100

data_string = 'abm3m4m8' # Ask, bid, 50 day moving avg. 200 day moving avg., percentage change from 50 day moving avg.
data_string = 'abm3m4m8' # Ask, bid, 50 day moving avg. 200 day moving avg., percentage change from 50 day moving avg.
## Data aquirement

# Online module
pull_historical_data('UWTI',data_string, directory="SP")
pull_historical_data('UGAZ',data_string, directory="SP")

# History module
with open('D:/new_stuff/SP/UWTI.csv', 'rb') as f:
    reader = csv.reader(f)
    for row in reader:
        this_data = row

ask_p = float(this_data[0])
bid_p = float(this_data[1])
avg_50 = float(this_data[2])
avg_200 = float(this_data[3])
avg_d50 = p2f(this_data[4])


percentage_o_array = (range(-100, 100, 5))
myInt = 100
percentage_array = [x / float(myInt) for x in percentage_o_array]
time_now = str(datetime.datetime.now())
time_nowt = datetime.datetime.now()

# Read history message
hist_message_old = read_hist_message('hist.csv')
hist_message = []
add_this_message = []
if  hist_message_old:
    for item in hist_message_old:
        hist_message.append(item[0])

for val in (percentage_array):
    if avg_d50<val and avg_d50<0:
        message =  "The UWTI has down " + str(abs(val*100)) + "% compared to the 50 days avg. at "
        message_with_time = message + time_now
        if hist_message is None or message not in hist_message:
            add_this_message = message_with_time;
            hist_message.append(message)
        break;
    if avg_d50>val and avg_d50>0:
        message =  "The UWTI has up " + str(abs(val*100)) + "% compared to the 50 days avg. at "
        message_with_time = message + time_now
        if hist_message is None or message not in hist_message:
            add_this_message = message_with_time;
            hist_message.append(message)
        break;

time_now2 = datetime.datetime.now();
elapsed = time_now2 - time_nowt;
print elapsed

if elapsed < datetime.timedelta(minutes=1):
    print "clean history message"
    clear_hist_message('hist.csv')

print (time_now2 -time_nowt);

# Save history message
if add_this_message:
    add_message(add_this_message,'the_message.txt')

add_message_list(hist_message,'hist.csv')


#send_message('UWTI','the_message.txt')


stock_list = []

statistics = {}


