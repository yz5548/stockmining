__author__ = 'Yuhang'
import urllib
# http://victorliew.quora.com/3-Ways-to-programatically-download-stuff-from-Yahoo-Financials
# http://www.jarloo.com/yahoo_finance/ (tags)
base_url = "http://finance.yahoo.com/d/quotes.csv?s="
def make_url(ticker_symbol, specific):
    ss = base_url + ticker_symbol + '&f=' + specific
    return ss

output_path = "D:/new_stuff"
def make_filename(ticker_symbol, directory="SP"):
    return output_path + "/" + directory + "/" + ticker_symbol + ".csv"

def pull_historical_data(ticker_symbol, specific, directory="SP"):
    try:
        urllib.urlretrieve(make_url(ticker_symbol,specific), make_filename(ticker_symbol, directory))
    except:
        pass
