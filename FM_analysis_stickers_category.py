__author__ = 'Yuhang'
import csv
import re
import os
from collections import defaultdict
from add_message import *
from send_email import send_message

# Get the stickers
reader = csv.reader(open('all_stickers.csv', 'rb'))
stocks = list(reader)
data_string = 'r5r6m6m8'
output_path = "D:/new_stuff"
sum_num = 0
categories = defaultdict(list) # save it as a dictionary

# Put the data into different categories
for item in stocks:
    sum_num = sum_num + 1
    categories[item[1]].append(item[0]) #Put the stickers under categories.
# Read and analysis the data in the excel file
reader = csv.reader(open('stock_statistics.csv', 'rb'))
stock_with_value = list(reader)
new_stock_list = defaultdict(list)
for item in stock_with_value:
    last_string = item[1]
    matches = re.findall(r'\'(.+?)\'',last_string)
    numbers = []
    for i,val in enumerate(matches):
        if val == 'N/A':
            numbers.append(float(0))
        else:
            numbers.append(float(val.strip('%')))
    if numbers[0]==0:
        new_stock_list[item[0]].append(-1000)
    else:
        new_stock_list[item[0]].append((1/numbers[0])*(1-numbers[2]))

writer = csv.writer(open('temp_save.csv','wb'))
for key, value in new_stock_list.items():
    writer.writerow([key, value])

# Rank the result from low to high in each domain., may one sticker ~ 2 categories.
scores = defaultdict(dict) # THERE IS A PROBLEM HERE, BUT WE NEED TO CONTINUE.
list_score = defaultdict(list)
for item in stocks:
  #  sub_dic = {}
   # sub_dic = {new_stock_list[item[0]][0]:item[0]}
    try:
        scores[item[1]][new_stock_list[item[0]][0]] = item[0]
        list_score[item[1]].append([new_stock_list[item[0]]])
    except:
        pass

final_list = defaultdict(list)
send_final_list = defaultdict(list)
for each_line_index in list_score:
    price_list = list_score[each_line_index]
    processing_list = []
    for item in price_list:
        processing_list.append(item[0])
    processing_list.sort(reverse=True)
    for i in processing_list:
        final_list[each_line_index].append(scores[each_line_index][i[0]])
writer = csv.writer(open('final_save.csv','wb'))
for key, value in final_list.items():
    send_final_list[key] = value[0:6]

os.remove('First_message.txt')
# Add the messages to here.
add_this_message = 'This result is calculated through: 1/PEG*(1 - percentage deviation from the 200Days avg:)'
add_message(add_this_message,'First_message.txt')
add_this_message = 'Update: Real-time  Data Source: Yahoo: \n'
add_message(add_this_message,'First_message.txt')
with open ('First_message.txt', 'ab') as fp:
    for p in send_final_list.items():
        fp.write("%s:%s\n" % p)

send_message('First automatic generated stock list from your evil morningbug !','First_message.txt')