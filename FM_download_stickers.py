__author__ = 'Yuhang'
from Yahoo_first_step import pull_historical_data
import csv
import os
import os.path
from collections import defaultdict
## Download the sticker and all the wanted data, save them a csv file with discipline. compute the time cost.

# Get the stickers
reader = csv.reader(open('all_stickers.csv', 'rb'))
stocks = list(reader)
data_string = 'r5r6m6m8' # PEG, Price/EPS current year, change from 200 day moving average, change from 50 day moving average.
output_path = "D:/new_stuff"
sum_num = 0
categories = defaultdict(list) # save it as a dictionary

for item in stocks:
    sum_num = sum_num + 1


# Download one
ctt = 0
for item in stocks:
    ctt = ctt + 1
    percentage = ctt/ float(sum_num)
    if (ctt%10)==0:
        print 'Complete {:.3%}: {}/{}'.format(percentage,ctt,sum_num)

    pull_historical_data(item[0],data_string, directory="SP")
    csv_name = output_path + '/SP/' + item[0] + '.csv'

    if os.path.isfile(csv_name):
        with open(csv_name, 'rb') as csvfile:
            reader2 = csv.reader(csvfile)
            this_line = list(reader2)
        if this_line:
            writer = csv.writer(open('stock_statistics2.csv', 'ab'))
            this_line.insert(0,item[0])
            writer.writerow(this_line)
     #   print this_line
            os.remove(csv_name)


# Delete the previous one

# Return the speed of download